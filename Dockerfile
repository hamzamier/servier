FROM python:3.8

ARG GITLAB_PIP_TOKEN

COPY ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt

# Copy local code to the container image.
COPY . /app
WORKDIR /app

ENTRYPOINT ["python", "-m", "main"]
