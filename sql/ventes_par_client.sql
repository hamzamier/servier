WITH
transaction_table AS (
    SELECT
        client_id,
        prod_id AS product_id,
        PARSE_DATE("%d/%m/%y", date) AS date,
        prod_price * prod_qty AS total_price
    FROM
        `servier.transactions`
    WHERE
        date BETWEEN '2019-01-01' AND '2019-12-31'
),
product_table AS (
    SELECT
        prod_id AS product_id,
        product_type
    FROM
        `servier.product_nomenclature`
    WHERE
        product_type IN ("MEUBLE", "DECO")
),
intermediate AS (
    SELECT
        tt.client_id,
        tt.total_price,
        pt.product_type
    FROM
        transaction_table tt
    JOIN
        product_table pt
    ON
        tt.product_id = pt.product_id
)
SELECT
    client_id,
    SUM(CASE WHEN product_type = "MEUBLE" THEN total_price ELSE 0 END) AS total_amount_meuble,
    SUM(CASE WHEN product_type = "DECO" THEN total_price ELSE 0 END) AS total_amount_deco
FROM
    intermediate
GROUP BY
    client_id;
