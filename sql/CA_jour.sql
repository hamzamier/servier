WITH
transaction_table AS (
  SELECT
    PARSE_DATE("%d/%m/%y", date) AS date,
    prod_price * prod_qty AS total_price
  FROM
    `dataset_servier.transactions` 
  WHERE
    date BETWEEN '2019-01-01' AND '2019-12-31'
)
SELECT
  date AS d_date,
  SUM(total_price) AS total_sale_amount
FROM
  transaction_table
GROUP BY
  date
ORDER BY
  d_date;
