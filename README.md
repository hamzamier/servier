# Test Technique
## Projet
- Readme.md
- .gitignore
- Dockerfile
- requirements.txt
- main.py
- servier/
    - data/ : données sources
    - src/
        - preprocessing/
            - preprocessing.py
    - iac/
        - bucket : création de bucket sur gcp
        - compute_engine : création d'une VM sur GCP
        - upload_files : chargement de données sur le bucket via terraform

    - sql/ : requêtes sql

## Réponses: 

### Question 4 : 
- créer une API qui permet de retourner le journal qui mentionne le plus de médicament en partant de fichier JSON_OUTPUT


### Question 6 : 

- Quels sont les éléments à considérer pour faire évoluer votre code afin qu’il puisse gérer de grosses
volumétries de données (fichiers de plusieurs To ou millions de fichiers par exemple) ?

    - Évaluer les performances d'exécution et détecter les zones du code nécessitant une optimisation.
    - Considérer l'utilisation de bibliothèques plus performantes spécifiques au traitement de texte.
    - Envisager des solutions sur Google Cloud Platform (GCP) telles que l'exportation des données vers BigQuery ou      
        l'utilisation du service DataProc avec PySpark pour une meilleure adaptation aux volumes de données massifs.

- Pourriez-vous décrire les modifications qu’il faudrait apporter, s’il y en a, pour prendre en considération de
telles volumétries ?
    - créer un cluster sur dataproc
    - refactory le code et intégrer apache spark
    - supprimer les ressource non utilisées 


![My Image](archi_servier.png)

## Application
### 1- Setup


#### Virtual environment
- Installer les packages nécessaires dans votre environnement
    ```bash
    pip install -r requirements.txt  
    ```
- Exécuter l'application
    ```bash
    python main.py 
    ```

#### Docker image

- Build l'image Docker
    ```bash
    docker build . -t servier
    ```
- Run l'image Docker
    ```bash
    docker run servier
    ```


### PyTest 

```bash
python -m  pytest 
```

### 2- SQL

  * 1 : CA_jour.sql
  * 2 : ventes_par_client.sql 
  





