provider "google" {
  project     = "second-inquiry-383810"
  region      = "europe-west1"  # Remplacez par la région de votre choix
  zone    = "europe-west1-b"
}

resource "google_storage_bucket" "my_bucket" {
  name     = "serviers"  # Remplacez par un nom unique
  location = "EU"  # Remplacez par la région de votre choix

  versioning {
    enabled = true
  }
}
