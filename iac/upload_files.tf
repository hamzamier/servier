resource "null_resource" "upload_files" {
  triggers = {
    always_run = "${timestamp()}"
  }

  provisioner "local-exec" {
    command = <<EOT
      gsutil cp ../data/drugs.csv gs://${google_storage_bucket.my_bucket.name}/
      gsutil cp ../data/clinical_trials.csv gs://${google_storage_bucket.my_bucket.name}/
      gsutil cp ../data/pubmed.csv gs://${google_storage_bucket.my_bucket.name}/
      gsutil cp ../data/pubmed.json gs://${google_storage_bucket.my_bucket.name}/

    EOT
  }
}
