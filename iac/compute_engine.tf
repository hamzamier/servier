resource "google_project_service" "compute" {
  project = "second-inquiry-383810"
  service = "compute.googleapis.com"
}

resource "google_compute_instance" "serviers" {
  name         = "serviers"
  machine_type = "e2-micro" # Instance de petite taille

  boot_disk {
    initialize_params {
      image = "projects/debian-cloud/global/images/debian-11-bullseye-v20230814" # Image Debian 9
    }
  }

  network_interface {
    network = "default" # Réseau par défaut
  }
}