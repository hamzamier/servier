from src.import_data import importData

import pandas as pd
import json
import re

def search_drug(x, df, field):
    """Get the DF of publications associated with the drug.

    Args:
        x (str): drug
        df (_type_): dataframe containing publications
        field (_type_): column name associated with the title of the publication

    Returns:
        _type_: dataframe
    """
    result = df[df[field].str.contains(x, case=False)]
    result = result.reset_index(drop=True)
    return result



def format_match(df):
    """Convert result df to json.

    Args:
        df (_type_): dataframe

    Returns:
        _type_: json
    """
    df = df.drop_duplicates()
    results = []
    for date in list(df["date"].unique()):
        value = {}
        value["date"] = date
        value["value"] = json.loads(df[df.date == str(date)].drop(
            "date", axis=1).to_json(orient="records"))
        results.append(value)
    return results



def data_pipeline():
    """Data pipeline to obtain an association between drug and publication."""
    import_data = importData()
    drugs = import_data.import_data_csv('data/drugs.csv')
    clinical_trials = import_data.import_data_csv("data/clinical_trials.csv")
    pubmed_csv = import_data.import_data_csv("data/pubmed2.csv")
    pubmed_json = import_data.import_data_json("data/pubmed.json")
    pubmed = pd.concat([pubmed_json, pubmed_csv]).astype(str)
 
    json_output = []
    json_drugs = json.loads(drugs.to_json(orient="records"))

    for drug in json_drugs:

        x = drug["drug"]
        result = drug

        pubmed_results = search_drug(
            x, pubmed[["date", "title", "id", "journal"]], field="title")
        result["pubmed"] = format_match(pubmed_results)

        clinical_trials_results = search_drug(x, clinical_trials[[
            "date", "scientific_title", "id", "journal"]],
            field="scientific_title")
        result["clinical_trials"] = format_match(
            clinical_trials_results)

        journal_result = search_drug(x, pubmed[["date", "title", "id", "journal"]],
                                                field="journal")
        result["journal"] = format_match(
            journal_result)  # .to_json(orient="records")
        json_output.append(result)
    with open("output/json_output.json", "w") as json_file:
    # Utilisez la fonction dump pour exporter le dictionnaire dans le fichier JSON
        json.dump(json_output, json_file)
    print(json_output)

    return json_output

