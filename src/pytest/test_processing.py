import unittest
import pandas as pd

# Importez votre fonction search_drug ici
from src.processing import search_drug

class TestSearchDrugFunction(unittest.TestCase):

    def setUp(self):
        # Créez un DataFrame de test
        data = {'title': ['Lorem Ipsum', 'Python is great', 'CASE Insensitive']}
        self.test_df = pd.DataFrame(data)

    def test_search_drug_case_sensitive(self):
        # Testez la recherche sensible à la casse
        result = search_drug('python', self.test_df, 'title')
        expected = pd.DataFrame({'title': ['Python is great']})

        self.assertTrue(result.equals(expected))

    def test_search_drug_case_insensitive(self):
        # Testez la recherche insensible à la casse
        result = search_drug('PYTHON', self.test_df, 'title')
        expected = pd.DataFrame({'title': ['Python is great']})
        self.assertTrue(result.equals(expected))

    def test_search_drug_empty_result(self):
        # Testez lorsque la recherche ne donne aucun résultat
        result = search_drug('java', self.test_df, 'title')
        expected = pd.DataFrame(columns=['title'])
        self.assertTrue(result.equals(expected))

if __name__ == '__main__':
    unittest.main()
