import pandas as pd
import json5

class importData:
    def import_data_csv(self, filename):
        """
        Import data from a CSV file and return a pandas DataFrame.
        Args:
            filename (str): The path to the CSV file.
        Returns:
            pd.DataFrame: The pandas DataFrame containing the CSV data.
        """
        try:
            # Read the CSV file using pandas
            data = pd.read_csv(filename)
            return data
        except FileNotFoundError:
            print(f"The file {filename} was not found.")
            return None
        except Exception as e:
            print(f"An error occurred while importing the CSV file: {str(e)}")
            return None

    def import_data_json(self, filename):
        """
        Import data from a JSON file and return a pandas DataFrame.
        Args:
            filename (str): The path to the JSON file.
        Returns:
            pd.DataFrame: The pandas DataFrame containing the JSON data.
        """
        try:
            # Read the JSON file using pandas
            with open(filename) as file:
                json_file = json5.load(file)
                data = pd.json_normalize(json_file)
            return data
        except FileNotFoundError:
            print(f"The file {filename} was not found.")
            return None
        except Exception as e:
            print(f"An error occurred while importing the JSON file: {str(e)}")
            return None



# Example of usage
if __name__ == "__main__":
    data_importer = importData()

    # Import data from a CSV file
    df_csv = data_importer.import_data_csv("data/drugs.csv")

    if df_csv is not None:
        print("CSV Data:")
        print(df_csv.head())

    # Import data from a JSON file
    df_json = data_importer.import_data_json("data/pubmed.json")

    if df_json is not None:
        print("\nJSON Data:")
        print(df_json.head())

  