from google.cloud import storage  # Pour Cloud Storage (installez google-cloud-storage via pip)
import jso


def export_data_json(self, json_data, output_filename, bucket_name):
    """
    Exporte un JSON vers Cloud Storage.
    Args:
        json_data (dict): Le JSON à exporter.
        output_filename (str): Le nom du fichier JSON de sortie.
        bucket_name (str): Le nom du bucket Cloud Storage.
    Returns:
        str: Un message confirmant que le fichier a été chargé avec succès.
    """
    # Conversion du JSON en chaîne JSON
    json_str = json.dumps(json_data)

    # Écriture du JSON dans un fichier local
    with open(output_filename, 'w') as json_file:
        json_file.write(json_str)

    # Envoi du fichier JSON vers Cloud Storage
    client = storage.Client()
    bucket = client.bucket(bucket_name)
    blob = bucket.blob(output_filename)
    blob.upload_from_filename(output_filename)

    return f"Le fichier {output_filename} a été chargé avec succès dans le bucket {bucket_name}."